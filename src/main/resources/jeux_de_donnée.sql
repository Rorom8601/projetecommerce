INSERT INTO category (name) VALUES ("petit électoménager"),("ordinateur"),("télephonie"),("loisir"),("jeux"),("divers");

INSERT INTO product (description, name, price, category_id, long_description) VALUES 
("description ordi 1", "ordi1", 999.50, 2, "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description ordi 2", "ordi2", 799.50, 2,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description machine à laver", "machine à laver", 399.50, 1,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description frigo", "frigo", 559.90, 1,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description telephone1", "telephone1", 159.50, 3,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description telephone2", "telephone2", 99.50, 3,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description drone", "Dji mavic 2 pro", 450.50, 4,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description voiture télécommandée", "voiture télécommandée", 99.50, 5,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description jeux vidéo", "jeux vidéo", 75.50, 5,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description console", "console", 499.50, 5,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description divers 1", "ordi1", 99.50, 6,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description divers 2", "ordi2", 49.50, 6,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?"),
("description divers 3", "ordi3", 29.50, 6,"Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident quidem, quia voluptatem aliquid nam, nisi eligendi quibusdam minus qui quasi consectetur doloremque unde? Exercitationem id rerum enim veritatis culpa ad?");

INSERT INTO image (name, url, product_id)VALUES ("img_ordi","https://cdn.pixabay.com/photo/2014/09/24/14/29/macbook-459196_960_720.jpg",1),
("img_ordi2","https://cdn.pixabay.com/photo/2015/06/24/15/45/hands-820272__340.jpg",2),
("img machine à laver", "https://media.istockphoto.com/photos/laundry-room-with-a-washing-machine-picture-id1310076735?b=1&k=20&m=1310076735&s=170667a&w=0&h=c7ZklvTX499ylTYXiXWjVwDu5MniSwfR6_U5kn2vFXI=",3),
("img frigo", "https://media.istockphoto.com/photos/woman-at-home-adding-things-to-her-shopping-list-while-checking-the-picture-id1326651903?b=1&k=20&m=1326651903&s=170667a&w=0&h=ngtMx_sdNqRFRQ98WaCJ5EWOKPbFpAweSzEphKGpryY=",4),
("img telephone","https://cdn.pixabay.com/photo/2016/11/29/12/30/phone-1869510__340.jpg",5),
("img telephone 2","https://cdn.pixabay.com/photo/2017/04/03/15/52/mobile-phone-2198770__340.png",6),
("img drone","https://cdn.pixabay.com/photo/2016/11/29/02/07/drone-1866742__340.jpg",7),
("img voiture","https://cdn.pixabay.com/photo/2017/07/06/15/22/rc-car-2478358__340.jpg",8),
("img jeux video","https://cdn.pixabay.com/photo/2019/04/12/08/44/pacman-4121590__340.png",9),
("img console", "https://cdn.pixabay.com/photo/2017/04/04/18/14/video-game-console-2202634__340.jpg",10),
("img rasoir", "https://cdn.pixabay.com/photo/2016/11/23/06/46/razor-1852087__340.png",11),
("img ventilateur", "https://cdn.pixabay.com/photo/2015/09/05/23/10/black-926117__340.jpg",13);

INSERT INTO account (birth_date, email, first_name, last_name, password, phone_number, role) VALUES
('1990-02-02',"rom@mail.com","Romain", "Durant","1234", "0665362542","ROLE_ADMIN"),
('1988-04-05',"@mail.com","Greg", "Durant","ABCD", "0665364568","ROLE_USER"),
('2002-03-10',"ame@mail.com","Amelie", "Durant","4567", "0665362500","ROLE_USER"),
('1984-01-02',"louloute@mail.com","Lou", "Durant","AERTY", "0665362599","ROLE_USER");

INSERT INTO address (address_detail, city, country, zip_code, account_id) VALUES 
("2, rue du stade", "Lyon", "France", "69000",1),
("2 bis, Grande Rue", "Meximieux", "France", "01250",2),
("4, rue Gambetta", "Paris", "France", "75000",3),
("12, rue des Ormes", "Poitiers", "France", "86000",4);

INSERT INTO order_table (amount, date, order_number,status, validated) VALUES (15, "2022-01-01", 115, "en cours", 0);

INSERT INTO order_line(price,quantity, order_id, product_id) VALUES (50, 5,1,3);

select * from product;

SELECT * from order_line;



