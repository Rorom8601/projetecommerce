package co.simplon.promo18.e_commerce.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import co.simplon.promo18.e_commerce.repository.AccountRepository;

@Service
public class AuthService  implements UserDetailsService{

  @Autowired
  private AccountRepository repo;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    return repo.findByEmail(username)
    .orElseThrow(() -> new UsernameNotFoundException("User not found (just like in the exercise)"));
  }
  
}
