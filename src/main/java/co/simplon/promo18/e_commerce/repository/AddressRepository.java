package co.simplon.promo18.e_commerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.e_commerce.entities.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
  
  List<Address> getAddressesByAccountId(int accountId);
}
