package co.simplon.promo18.e_commerce.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.e_commerce.entities.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account,Integer> {
  Optional<Account> findByEmail(String email);
}
