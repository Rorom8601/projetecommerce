package co.simplon.promo18.e_commerce.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.e_commerce.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findByCategoryId(Integer id);

    Page<Product> findByNameContainingOrDescriptionContainingOrLongDescriptionContaining(String name, String description, String longDescription, Pageable pageable);

}
