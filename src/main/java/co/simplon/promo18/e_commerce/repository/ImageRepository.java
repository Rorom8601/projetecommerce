package co.simplon.promo18.e_commerce.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.e_commerce.entities.Image;
import co.simplon.promo18.e_commerce.entities.Product;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {
  List<Image> findByProduct(Product product);
}
