package co.simplon.promo18.e_commerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.e_commerce.entities.OrderLine;

@Repository
public interface OrderLineRepository extends JpaRepository<OrderLine, Integer> {
  
}
