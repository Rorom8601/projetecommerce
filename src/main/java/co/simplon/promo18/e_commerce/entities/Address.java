package co.simplon.promo18.e_commerce.entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Address {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @NotBlank
  private String addressDetail;
  @NotBlank
  private String city;
  @NotBlank
  private String zipCode;
  @NotBlank
  private String country;

  @OneToMany(mappedBy = "deliveryAddress")
  @JsonIgnore
  private List<Order> deliveryAddresses=new ArrayList<>();

  @OneToMany(mappedBy = "billingAddress")
  @JsonIgnore
  private List<Order> billingAddresses=new ArrayList<>();

  @ManyToOne
  @JsonIgnore
  private Account account;

  public Address() {}
  public Address(String addressDetail, String city, String zipCode, String country) {
    this.addressDetail = addressDetail;
    this.city = city;
    this.zipCode = zipCode;
    this.country = country;
  }
  public Address(Integer id, String addressDetail, String city, String zipCode, String country) {
    this.id = id;
    this.addressDetail = addressDetail;
    this.city = city;
    this.zipCode = zipCode;
    this.country = country;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getAddressDetail() {
    return addressDetail;
  }
  public void setAddressDetail(String addressDetail) {
    this.addressDetail = addressDetail;
  }
  public String getCity() {
    return city;
  }
  public void setCity(String city) {
    this.city = city;
  }
  public String getZipCode() {
    return zipCode;
  }
  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }
  public String getCountry() {
    return country;
  }
  public void setCountry(String country) {
    this.country = country;
  }
  public List<Order> getDeliveryAddresses() {
    return deliveryAddresses;
  }
  public void setDeliveryAddresses(List<Order> deliveryAddresses) {
    this.deliveryAddresses = deliveryAddresses;
  }
  // public List<Order> getBillingAddresses() {
  //   return billingAddresses;
  // }
  // public void setBillingAddresses(List<Order> billingAddresses) {
  //   this.billingAddresses = billingAddresses;
  // }
  // public Account getAccount() {
  //   return account;
  // }
  public void setAccount(Account account) {
    this.account = account;
  }
  
}
