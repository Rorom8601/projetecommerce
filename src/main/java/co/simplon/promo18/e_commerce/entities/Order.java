package co.simplon.promo18.e_commerce.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "order_table")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private int orderNumber;
  private LocalDate date;
  @NotBlank
  private String status;
  @NotNull
  private boolean validated;
  @NotNull
  private double amount;

  @OneToMany(mappedBy = "order")
  @JsonIgnoreProperties("order")
  private List<OrderLine> orderLines = new ArrayList<>();

  @ManyToOne
  @JsonIgnore
  private Account account;

  @ManyToOne
  @JsonIgnoreProperties("deliveryAddresses")
  private Address deliveryAddress;

  @ManyToOne
  @JsonIgnoreProperties("billingAddresses")
  private Address billingAddress;



  public Order() {}

  public Order(int orderNumber, LocalDate date, String status, boolean validated, double amount) {
    this.orderNumber = orderNumber;
    this.date = date;
    this.status = status;
    this.validated = validated;
    this.amount = amount;
  }

  public Order(Integer id, int orderNumber, LocalDate date, String status, boolean validated,
      double amount) {
    this.id = id;
    this.orderNumber = orderNumber;
    this.date = date;
    this.status = status;
    this.validated = validated;
    this.amount = amount;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(int orderNumber) {
    this.orderNumber = orderNumber;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public boolean isValidated() {
    return validated;
  }

  public void setValidated(boolean validated) {
    this.validated = validated;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public List<OrderLine> getOrderLines() {
    return orderLines;
  }

  public void setOrderLines(List<OrderLine> orderLines) {
    this.orderLines = orderLines;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public Address getDeliveryAddress() {
    return deliveryAddress;
  }

  public void setDeliveryAddress(Address deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }

  public Address getBillingAddress() {
    return billingAddress;
  }

  public void setBillingAddress(Address billingAddress) {
    this.billingAddress = billingAddress;
  }

}
