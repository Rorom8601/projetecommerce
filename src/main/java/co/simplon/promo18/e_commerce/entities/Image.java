package co.simplon.promo18.e_commerce.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Image {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private String name;
  @NotBlank
  private String url;
  
  @ManyToOne
  @JsonIgnoreProperties("images")
  private Product product;

  public Image() {}

  public Image(String name, String url) {
    this.name = name;
    this.url = url;
  }

  public Image(Integer id, String name, String url) {
    this.id = id;
    this.name = name;
    this.url = url;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public void setQuantity(String name2) {
  }

public void setPrice(String url2) {
}

}
