package co.simplon.promo18.e_commerce.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
public class Account implements UserDetails {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @NotBlank
  private String firstName;
  @NotBlank
  private String lastName;
  private String phoneNumber;
  @NotBlank
  @Email
  private String email;
  @NotBlank
  private String password;
  @Past
  @NotNull
  private LocalDate birthDate;

  private String role;

  @OneToMany(cascade =CascadeType.REMOVE,mappedBy = "account")
  @JsonIgnore
  private List<Order> orders = new ArrayList<>();

  @OneToMany(cascade =CascadeType.REMOVE, mappedBy = "account")
  @JsonIgnore
  private List<Address> addresses = new ArrayList<>();


  public Account() {}

  public Account(@NotBlank String firstName, @NotBlank String lastName, String phoneNumber,
      @NotBlank @Email String email, @NotBlank String passWord, @Past @NotNull LocalDate birthDate,
      String role, List<Order> orders, List<Address> addresses) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.password = passWord;
    this.birthDate = birthDate;
    this.role = role;
    this.orders = orders;
    this.addresses = addresses;
  }

  public Account(Integer id, @NotBlank String firstName, @NotBlank String lastName,
      String phoneNumber, @NotBlank @Email String email, @NotBlank String passWord,
      @Past @NotNull LocalDate birthDate, String role, List<Order> orders,
      List<Address> addresses) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.password = passWord;
    this.birthDate = birthDate;
    this.role = role;
    this.orders = orders;
    this.addresses = addresses;
  }


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassWord(String password) {
    this.password = password;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public List<Order> getOrders() {
    return orders;
  }

  public void setOrders(List<Order> orders) {
    this.orders = orders;
  }

  public List<Address> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<Address> addresses) {
    this.addresses = addresses;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return List.of(new SimpleGrantedAuthority(role));
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

}

