package co.simplon.promo18.e_commerce.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class OrderLine {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @NotNull
  private int quantity;
  @NotNull
  private double price;
  
  @ManyToOne
  @JsonIgnoreProperties("orderLines")
  private Order order;

  @ManyToOne
  @JsonIgnoreProperties("orderLines")
  private Product product;

  public OrderLine() {}
  public OrderLine(int quantity, double price) {
    this.quantity = quantity;
    this.price = price;
  }
  public OrderLine(Integer id, int quantity, double price) {
    this.id = id;
    this.quantity = quantity;
    this.price = price;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public int getQuantity() {
    return quantity;
  }
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
  public double getPrice() {
    return price;
  }
  public void setPrice(double price) {
    this.price = price;
  }
  public Order getOrder() {
    return order;
  }
  public void setOrder(Order order) {
    this.order = order;
  }
  public Product getProduct() {
    return product;
  }
  public void setProduct(Product product) {
    this.product = product;
  }
}
