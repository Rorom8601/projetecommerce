package co.simplon.promo18.e_commerce.Controller;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.e_commerce.entities.OrderLine;
import co.simplon.promo18.e_commerce.repository.OrderLineRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/orderline")
public class OrderLineController {
  
  
  @Autowired
  private OrderLineRepository orRepo;

  @GetMapping
  public List<OrderLine>getAll() {
    return orRepo.findAll();
  }
  @GetMapping("/{id}")
  public OrderLine getOrderLineById(@PathVariable int id) {
    OrderLine orderLine =
        orRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    return orderLine;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public OrderLine addOrderLine(@RequestBody OrderLine orderLine) {
    orderLine.setId(null);
    return orRepo.save(orderLine);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteOrderLineById(@PathVariable int id) {
    orRepo.deleteById(id);

  }
  @PutMapping("/{id}")
  public OrderLine update(@PathVariable int id, @Valid @RequestBody OrderLine orderline) {
    OrderLine toUpdate = getOrderLineById(id);
    toUpdate.setQuantity(orderline.getQuantity());
    toUpdate.setPrice(orderline.getPrice());
    return orRepo.save(toUpdate);
}
}