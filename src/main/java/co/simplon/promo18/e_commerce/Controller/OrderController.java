package co.simplon.promo18.e_commerce.Controller;

import java.time.LocalDate;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.e_commerce.entities.Account;
import co.simplon.promo18.e_commerce.entities.Order;
import co.simplon.promo18.e_commerce.repository.AccountRepository;
import co.simplon.promo18.e_commerce.repository.OrderRepository;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/api/order")
public class OrderController {

  @Autowired
  OrderRepository orderRepository;

  @Autowired
  AccountRepository acRepo;

  @GetMapping("/{id}")
  public Order getOrderById(@PathVariable int id){
    Order order = orderRepository.findById(id)
    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
return order;
  }

  @PostMapping("/{idAccount}")
  @ResponseStatus(HttpStatus.CREATED)
  public void saveOrder(@Valid @RequestBody Order order,@PathVariable int idAccount) {
   order.setId(null);
   order.setDate(LocalDate.now());
   Account account=acRepo.findById(idAccount).get();
   order.setAccount(account);
   //order.setOrderNumber(order.getId());
   orderRepository.save(order);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public Order deleteOrder(@PathVariable int id){
    Order orderTodelete=getOrderById(id);
    orderRepository.delete(orderTodelete);
    return orderTodelete;
  }
    
  
}
