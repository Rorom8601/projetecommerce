package co.simplon.promo18.e_commerce.Controller;


import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.e_commerce.entities.Image;
import co.simplon.promo18.e_commerce.entities.OrderLine;
import co.simplon.promo18.e_commerce.entities.Product;
import co.simplon.promo18.e_commerce.repository.CategoryRepository;
import co.simplon.promo18.e_commerce.repository.ImageRepository;
import co.simplon.promo18.e_commerce.repository.OrderLineRepository;
import co.simplon.promo18.e_commerce.repository.ProductRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/product")
public class ProductController {

  @Autowired
  private ProductRepository proRepo;

  @Autowired CategoryRepository catRepo;

  @Autowired
  private OrderLineRepository orderRepo;

 @Autowired
 private ImageRepository imRepo;



  // @GetMapping
  // public List<Product> getAll() {
  //   return proRepo.findAll();
  // }

  @GetMapping
  public Page<Product> getAll(
  @RequestParam(required = false, defaultValue = "0") int page,
  @RequestParam(required = false, defaultValue = "10") int pageSize) {
      return proRepo.findAll(PageRequest.of(page, pageSize));
  }

  @GetMapping("/{id}")
  public Product getProductById(@PathVariable int id) {
    Product product =
        proRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    return product;
    
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Product addProduct(@Valid @RequestBody Product product) {
    product.setId(null);

     proRepo.save(product);
    for (Image image  : product.getImages()) {
      image.setProduct(product);
      imRepo.save(image);
    }

     return product;
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteProductById(@PathVariable int id) {
    Product productToDelete = getProductById(id);
    List<OrderLine> orderLines = orderRepo.findAll();
    for (OrderLine o : orderLines) {
      o.setProduct(null);
    }
    proRepo.delete(productToDelete);
  }

  @PutMapping("/{id}")
  public Product update(@PathVariable int id, @Valid @RequestBody Product product) {
    Product toUpdate = getProductById(id);
    toUpdate.setName(product.getName());
    toUpdate.setDescription(product.getDescription());
    toUpdate.setPrice(product.getPrice());
    return proRepo.save(toUpdate);
  }

  @GetMapping("/category/{id}")
  public List<Product> getProductsByCategory(@PathVariable int id){
    List<Product> products=proRepo.findByCategoryId(id);
    return products;
  }

  @GetMapping("search/{input}")
  public Page<Product> getProductListBySearch(@PathVariable String input,@RequestParam(required = false, defaultValue = "0") int page,
  @RequestParam(required = false, defaultValue = "10") int pageSize){
    Page<Product> products=proRepo.findByNameContainingOrDescriptionContainingOrLongDescriptionContaining(input, input, input,PageRequest.of(page, pageSize));
    return products;
  }

  

}
