package co.simplon.promo18.e_commerce.Controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.e_commerce.entities.Category;
import co.simplon.promo18.e_commerce.entities.Product;
import co.simplon.promo18.e_commerce.repository.CategoryRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/category")

public class CategoryController {

  @Autowired
  private CategoryRepository catRepo;



  @GetMapping("/{id}")
  public Category getCategoryById(@PathVariable int id) {
    Category category =
        catRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    return category;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Category addCategory(@Valid @RequestBody Category category) {
    category.setId(null);
    return catRepo.save(category);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteCategoryById(@PathVariable int id) {
    Category categoryToDelete = getCategoryById(id);
    catRepo.delete(categoryToDelete);
  }

  @PutMapping("update/{idCategory}")
  public void updateProductList(@PathVariable int idCategory, @RequestBody Product product) {
    Category cat = getCategoryById(idCategory);
    cat.getProducts().add(product);
  }



  @PutMapping("/{id}")
  public Category update(@PathVariable int id, @Valid @RequestBody Category category) {
    Category toUpdate = getCategoryById(id);
    toUpdate.setName(category.getName());
    return catRepo.save(toUpdate);
  }

  @GetMapping
  public List<Category> getAll() {
    return catRepo.findAll();
  }
}


