package co.simplon.promo18.e_commerce.Controller;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.e_commerce.entities.Account;
import co.simplon.promo18.e_commerce.entities.Address;
import co.simplon.promo18.e_commerce.repository.AccountRepository;
import co.simplon.promo18.e_commerce.repository.AddressRepository;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/address")
public class AddressController {

  @Autowired
  AddressRepository adRepo;

  @Autowired 
  AccountRepository  accountRepo;

  @GetMapping("/{id}")
  public Address getAddressById(@PathVariable int id) {
    Address address =
        adRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    return address;
  }

  @GetMapping("/account/{idAccount}")
  public List<Address> getAddressesByAccountId(@PathVariable int idAccount){
  Account account=accountRepo.findById(idAccount).get();
  List<Address> list1=account.getAddresses();
  // List<Address> list= adRepo.getAddressesByAccountId(idAccount);
    return list1;

  }

  @PostMapping("/{idAccount}")
  @ResponseStatus(HttpStatus.CREATED)
  public Address saveAddress(@Valid @RequestBody Address address, @PathVariable int idAccount) {
    address.setId(null);
    //enregisment du compte réferancant l'adresse pour remplir la colonne accountId dans adresse
    Account accountToUpdate=accountRepo.findById(idAccount).get();
    address.setAccount(accountToUpdate);
   // accountRepo.save(accountToUpdate);

    return adRepo.save(address);
  }
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteAddressById(@PathVariable int id){
      Address addressToDelete=getAddressById(id);
      adRepo.delete(addressToDelete);
  }



}
