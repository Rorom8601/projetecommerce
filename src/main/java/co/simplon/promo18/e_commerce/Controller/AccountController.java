package co.simplon.promo18.e_commerce.Controller;

import java.time.LocalDate;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.e_commerce.entities.Account;
import co.simplon.promo18.e_commerce.repository.AccountRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/account")
public class AccountController {

  @Autowired
  AccountRepository acRepo;

  @Autowired
    private PasswordEncoder encoder;


  @GetMapping("/{id}")
  public Account getAccountById(@PathVariable int id) {
   Optional< Account> account =acRepo.findById(id);
        if(account.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);   
        }
    return account.get();
  }

  @GetMapping("/user")
    public Account getAccount(@AuthenticationPrincipal Account account) {
        return account;
    }


  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Account saveAccount(@RequestBody @Valid Account account) {
    if(acRepo.findByEmail(account.getEmail()).isPresent()){
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "cet identifiant existe déja");
    }
    account.setId(null);
    account.setRole("ROLE_USER");
    String hashedPassword=encoder.encode(account.getPassword());
    account.setPassWord(hashedPassword);
    acRepo.save(account);
    SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(account, null, account.getAuthorities()));
    return account;
  }

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void updateAccount(@PathVariable int id, @RequestBody Account account){
    Account acToUpdate=getAccountById(id);
    acToUpdate.setFirstName(account.getFirstName());
    acToUpdate.setLastName(account.getLastName());
    acToUpdate.setPhoneNumber(account.getPhoneNumber());
    acToUpdate.setBirthDate(account.getBirthDate());
    acRepo.save(acToUpdate);
  }


  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteAccountById(@PathVariable int id){
      Account accountToDelete=getAccountById(id);
      acRepo.delete(accountToDelete);
  }

  @GetMapping("/email/{email}")
  public Account getByEmail(@PathVariable String email){
    if (acRepo.findByEmail(email).isPresent()) {
      return acRepo.findByEmail(email).get();
    } 
    else{
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }
}
