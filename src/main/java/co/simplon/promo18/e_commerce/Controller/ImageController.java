package co.simplon.promo18.e_commerce.Controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.e_commerce.entities.Account;
import co.simplon.promo18.e_commerce.entities.Image;
import co.simplon.promo18.e_commerce.repository.ImageRepository;

@RestController
@RequestMapping("api/image")
public class ImageController {
  
  @Autowired
  private ImageRepository imaRepo;

  @GetMapping
  public List<Image>getAll() {
    return imaRepo.findAll();
  }

@GetMapping("/{id}")
public Image getImageById(@PathVariable int id) {
  Image image =
      imaRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  return image;

  
}

@PostMapping
@ResponseStatus(HttpStatus.CREATED)
public Image addImageById(@Valid @RequestBody Image image) {
  image.setId(null);
  return imaRepo.save(image);
}

@DeleteMapping("/{id}")
@ResponseStatus(HttpStatus.NO_CONTENT)
public void deleteImage(@PathVariable int id) {
  imaRepo.deleteById(id);

}

@PutMapping("/{id}")
public Image update(@PathVariable int id, @Valid @RequestBody Image image) {
  Image toUpdate = getImageById(id);
  toUpdate.setName(image.getName());
  toUpdate.setUrl(image.getUrl());
  return imaRepo.save(toUpdate);
}
@PostMapping("/upload")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Image add(@ModelAttribute Image image, 
                    @ModelAttribute MultipartFile upload, 
                    @AuthenticationPrincipal Account account) {
        
        image.setId(null);

        
        String filename = UUID.randomUUID() + "."+ FilenameUtils.getExtension(upload.getOriginalFilename());
        
        try {
           
            upload.transferTo(new File(getUploadFolder(), filename));
           
            image.setUrl(filename);
            imaRepo.save(image);
        } catch (IllegalStateException | IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "File upload failed");
        }
        return image;
    }

     
    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
        if(!folder.exists()) {
            folder.mkdirs();
        }
        return folder;
            
    }
}
