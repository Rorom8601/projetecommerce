# Projet e-commerce :
## 1. Présentation : 

Nous avons décidé de réaliser un site de vente en ligne spécialisé dans la commercialisation des produits éléctroménagers.

## 2. Phase Conception :

Après concertation, Nous avons conçu un [diagramme de Classe](diagramme_de_classe_ecommerce.mdj) pour visualiser la stucture de notre site.  Ensuite, nous avons élaboré les différentes façons dont l'utilisateur peut intéragir avec le site (cf [diagramme de Use Case](digramme_de_use_case_%20ecommerce%20.pdf)).

Enfin, à l'aide du logiciel Figma, nous avons réalisé les [maquettes](wireframe_e-commerce.pdf).
 

## 3. Réalisation de projet :
### 3.1. Techniques utilisées :
***
Pour réaliser notre site de e-commerce, nous avons utilisé :
 
* [Spring Boot et Jpa](https://gitlab.com/Rorom8601/projetecommerce) pour le Backend  
* Et [Angular](https://gitlab.com/Rorom8601/e_commerce_angular) pour le Frontend.
  
### 3.2. Étapes de réalisation du site :
***
1. Tout d'abord, nous avons créé un projet commun sur GitLab afin de travailler en équipe. 
   


2.  Ensuite, nous avons réalisé 3 sprints puisque nous avons travaillé en méthode agile :
    *  Les deux premiers sprints ( sprint par deux semaines ) et le dernier (sprint par semaine).
    *  En début de chaque sprint, nous avons défini les fonctionnalités à livrer et avons assigné les tâches à réaliser sur le Board et ainsi nous avons créé les Issues sur le GitLab.
    * Presque chaque après-midi, nous faisions le point sur les difficultés rencontrées et échangions sur l'état d'avancement du projet.
    * À l'issue de chaque sprint, nous faisions une review avec le groupe et un RetEx. Avec une autre équipe de notre promo, nous faisions une démonstration des fonctionnalité dévollopées durant le sprint.
  
  
3.  Finalisation du projet : nous avons travaillé en pairprogramming afin de tester toutes les fonctionnalités et vérifier que le site marche bien. Ainsi, nous avons stylisé notre site à l'aide de **BootStrap**.
### 3.3. Points à signaler : 
*** 
* Faire une recherche par nom présent dans le titre, la description et dans la longue description.
* Panier en LocalStorage.
* Le fait que seuls les administrateurs (Rôle) peuvent ajouter, modifier ou encore supprimer produits et catégories.
* Recherche par catégorie.
* Autentification avec hashing de mot de passe.
* Nécessité d'être connecté pour créer un panier.
* création du compte authentification avec juste mot de passe et adresse mail, les autres champs sont entrés en dur avec des valeurs temporaires.
* À la validation de la commande, on demande à l'utilisateur de compléter les champs manquants.

* Exécuter le [jeu de données](jeux_de_donnée.sql)
   
### 3.4. RetEx de projet :
***

| Bien marché | Pas marché | à amiliorer | 
|:--------------|:-------------:|--------------:|
|répartition des taĉhes : tous le monde a codé. |On devait mettre en commun bien avant (problème de Jpa et Json Ignore) |- Anticiper |
|- On a tous progréssé|- Tester plus tôt|- Tests fonctionnels plus précoces|
|- Le live coding sur les  deux derniers jours a bien fonctionné (Romain à distance|- Romain a fait le Local Storage et l'aspect lié au panier en front pas été compris par le reste d el'équipe|- On aurait fait le panier Front en pairprogramming |
|- Bonne ambiance||- On aurait dû faire le Readme au fur et à mesure|


    









