# RetEx Sprint 1


## Bien passé
- Bonne vélocité de Sérigne et Mustapha
- Lead Romain OK
- Travail en binôme de Serigne et Mustapha efficace et pedagogique
- Tous le monde code

## Mal passé
-Mohammad en arrêt de travail ( pas de sa faute)

## A améliorer
- Mieux tenir le board
- Beaucoup d'infos ont été verbalisées mais pas toujours consignées

# RetEx Sprint 2

## Bien passé
- Bien avancé, bonne vélocité
- Bonne collaboration sur la pagination avec la création d'un composant "test" qui ne cassait pas l'ancien.
- Bonne communication
- Bonne entente

## Mal passé
- En distantiel , trop de salons portant le même nom, diifcile de s'y retrouver

## A améliorer
- Accélérer pour finir dans les délais
- Pas fait de board pour le sprint 2, uniquement de la communication orale, nous avons donc seulement 2 sprints ( le 2eme fait donc 15 jours). Y remédier.

